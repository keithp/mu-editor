mu-editor (1.2.2-1) unstable; urgency=medium

  * New upstream version
  * Optimize USB polling
  * Adopt package.

 -- Keith Packard <keithp@keithp.com>  Wed, 27 Sep 2023 16:43:46 -0700

mu-editor (1.0.3+dfsg-6) unstable; urgency=medium

  * d/patches:
    - Add patch disable-microbit-mount-tests (Closes: #1027378)

 -- Nick Morrott <nickm@debian.org>  Thu, 09 Feb 2023 05:17:47 +0000

mu-editor (1.0.3+dfsg-5) unstable; urgency=medium

  * d/control:
    - Declare compliance with Debian Policy 4.6.2 (no changes)
  * d/copyright:
    - Bump years of Debian copyright
  * d/mu-editor.lintian-overrides:
    - Refresh overrides
  * d/patches:
    - Add patch tests-reduce-builtin-mock-scope

 -- Nick Morrott <nickm@debian.org>  Thu, 09 Feb 2023 03:20:50 +0000

mu-editor (1.0.3+dfsg-4) unstable; urgency=medium

  * d/patches:
    - Add patch avoid-floats-for-int-arguments

 -- Nick Morrott <nickm@debian.org>  Tue, 17 May 2022 23:42:29 +0100

mu-editor (1.0.3+dfsg-3) unstable; urgency=medium

  * d/control:
    - Update standards version to 4.6.0, no changes needed.
    - Drop unnecessary version constraints for dependencies
  * d/copyright:
    - Refresh years of Debian copyright
    - Use 4-digit years for Debian copyright ranges
  * d/patches:
    - Add patch fix-missing-kernel-attr-qtkernelmanager (Closes: #1005259)
    - Add patch fix-semver-deprecation-warnings
  * d/s/lintian-overrides:
    - Add override for long lines in source
  * d/u/metadata:
    - Remove unknown field Bug-Repository
  * d/watch:
    - Update pattern for GitHub archive URLs

 -- Nick Morrott <nickm@debian.org>  Tue, 15 Feb 2022 01:39:04 +0000

mu-editor (1.0.3+dfsg-2) unstable; urgency=medium

  [ Debian Janitor ]
  * d/u/metadata:
    - Set upstream metadata fields: Bug-Database.
    - Remove obsolete fields Contact, Name (already present in
      machine-readable debian/copyright).

  [ Ondřej Nový ]
  * d/control:
    - Update Maintainer field with new Debian Python Team contact address.
    - Update Vcs-* fields with new Debian Python Team Salsa layout.

  [ Nick Morrott ]
  * d/control:
    - Declare compliance with Debian Policy 4.5.1
  * d/copyright:
    - Refresh lists of source files (redundant globbing patterns)
    - Refresh years of Debian copyright and contact email address
  * d/mu-editor.lintian-overrides:
    - Add override for Python egg metadata
  * d/salsa-ci.yml:
    - Add Salsa CI pipeline

 -- Nick Morrott <nickm@debian.org>  Sun, 07 Feb 2021 13:35:47 +0000

mu-editor (1.0.3+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.3+dfsg
  * d/control:
    - Declare compliance with Debian Policy 4.5.0
    - Bump debhelper compatibility level to 13
    - Add Suggests on python3-gpiozero (arch:any since buster)
  * d/patches:
    - Refresh patch soften-dependency-versions
    - Drop patch test_app_icon_as_string (applied upstream)

 -- Nick Morrott <nickm@debian.org>  Sat, 13 Jun 2020 06:12:46 +0100

mu-editor (1.0.2+dfsg-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.

  [ Nick Morrott ]
  * d/control:
    - Uploaders: use my new Debian address
    - Expand definition of "REPL" in long description
    - Mark mu-editor-doc as M-A: foreign
    - Add Rules-Requires-Root field
    - Depend on default python3 rather than python3-all
  * d/tests/mu-editor:
    - Run autopkgtests against default python3. Thanks to
      Steve Langasek for the patch (Closes: #943449)
  * d/patches:
    - add use-https-urls-in-appstream-data

 -- Nick Morrott <nickm@debian.org>  Mon, 04 Nov 2019 12:30:14 +0000

mu-editor (1.0.2+dfsg-3) unstable; urgency=medium

  * d/patches: add update-rtp-for-mu-debug (Closes: #930270)
    - adapted from patch from Peter Green

 -- Nick Morrott <knowledgejunkie@gmail.com>  Tue, 18 Jun 2019 01:34:00 +0100

mu-editor (1.0.2+dfsg-2) unstable; urgency=medium

  * d/gbp.conf: use pristine-tar
  * d/patches: add test_app_icon_as_string (Closes: #923402)

 -- Nick Morrott <knowledgejunkie@gmail.com>  Thu, 28 Feb 2019 02:43:16 +0000

mu-editor (1.0.2+dfsg-1) unstable; urgency=medium

  * New upstream version 1.0.2+dfsg
  * d/control: refresh (build) dependencies
  * d/{control,compat}: bump debhelper compatibility level to 12
  * d/patches: refresh patches (Closes: #919260)
  * d/{rules,tests/mu-editor}: restore test randomisation

 -- Nick Morrott <knowledgejunkie@gmail.com>  Wed, 16 Jan 2019 03:11:03 +0000

mu-editor (1.0.1+dfsg-1) unstable; urgency=medium

  * Initial release. (Closes: #901461)

 -- Nick Morrott <knowledgejunkie@gmail.com>  Mon, 31 Dec 2018 21:15:58 +0100
