Source: mu-editor
Section: python
Priority: optional
Maintainer: Python Applications Packaging Team <python-apps-team@lists.alioth.debian.org>
Uploaders: Nick Morrott <nickm@debian.org>, Keith Packard <keithp@keithp.com>
Build-Depends:
 black,
 debhelper-compat (= 13),
 dh-python,
 fonts-inconsolata,
 libjs-skeleton,
 node-normalize.css,
 pandoc,
 python3 (>= 3.6),
 python3-appdirs (>= 1.4.3),
 python3-coverage,
 python3-flask (>= 1.0.2),
 python3-matplotlib (>= 2.2.2),
 python3-mock,
 python3-nudatus,
 python3-pil,
 python3-pycodestyle (>= 2.4.0),
 python3-pyflakes (>= 2.0.0),
 python3-pyqt5 (>= 5.11.3),
 python3-pyqt5.qsci (>= 2.10.4),
 python3-pyqt5.qtchart (>= 5.10.1),
 python3-pyqt5.qtserialport (>= 5.11.3),
 python3-pytest,
 python3-pytest-cov,
# python3-pytest-faulthandler
 python3-pytest-random-order,
 python3-pytest-xvfb,
 python3-qtconsole (>= 4.3.1),
 python3-requests,
 python3-semver (>= 2.0.1),
 python3-serial (>= 3.4),
 python3-setuptools,
 python3-sphinx,
 python3-tk,
 python3-uflash,
Rules-Requires-Root: no
Standards-Version: 4.5.0
Homepage: https://codewith.mu/
Vcs-Browser: https://salsa.debian.org/python-team/applications/mu-editor
Vcs-Git: https://salsa.debian.org/python-team/applications/mu-editor.git

Package: mu-editor
Architecture: all
Depends:
 black,
 fonts-inconsolata,
 libjs-skeleton,
 node-normalize.css,
 python3 (>= 3.6),
 python3-guizero,
 python3-matplotlib,
 python3-pil,
 python3-pyqt5,
 python3-pyqt5.qsci,
 python3-pyqt5.qtchart,
 python3-pyqt5.qtserialport,
 python3-qtconsole,
 python3-requests,
 python3-uflash,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 mu-editor-doc,
 python3-gpiozero,
 python3-pigpio,
Description: simple editor for beginner Python programmers
 Mu is a simple code editor for beginner programmers, based on extensive
 feedback from teachers and learners. Having said that, Mu is for anyone
 who wants to use a simple "no frills" editor.
 .
 Mu is a modal editor with modes for:
 .
  * standard Python 3 (including a graphical debugger)
  * MicroPython on the BBC micro:bit
  * MicroPython on ESP8266/ESP32 boards
  * CircuitPython on Adafruit/Particle/Electronic Cats/SparkFun boards
  * Pygame Zero
  * Snek on various Arduino-compatible devices
  * Web development (using Flask)
 .
 Some modes provide read-eval-print loop (REPL) support, either
 running on a connected CircuitPython, MicroPython or Snek device, or
 as a Jupyter-based iPython session in Python3 mode.
 .
 This package contains the Mu editor. Detailed online user documentation
 and tutorials for the Mu editor are available from within the editor,
 or at https://codewith.mu

Package: mu-editor-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Suggests:
 www-browser,
Description: simple editor for beginner Python programmers (documentation)
 Mu is a simple code editor for beginner programmers, based on extensive
 feedback from teachers and learners. Having said that, Mu is for anyone
 who wants to use a simple "no frills" editor.
 .
 Mu is a modal editor with modes for:
 .
  * standard Python 3 (including a graphical debugger)
  * MicroPython on the BBC micro:bit
  * MicroPython on ESP8266/ESP32 boards
  * CircuitPython on Adafruit/Particle/Electronic Cats/SparkFun boards
  * Pygame Zero
  * Snek on various Arduino-compatible devices
  * Web development (using Flask)
 .
 Some modes provide read-eval-print loop (REPL) support, either
 running on a connected CircuitPython, MicroPython or Snek device, or
 as a Jupyter-based iPython session in Python3 mode.
 .
 This package contains the developer documentation for the Mu editor. Detailed
 user documentation and tutorials for the Mu editor are available from within
 the editor, or at https://codewith.mu
