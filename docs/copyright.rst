Copyright Information
---------------------

Mu Copyright
============

Mu, its source code and associated assets are copyright Nicholas H.Tollervey
and others (those who have made significant contributions to Mu can be found in
this list of :doc:`authors`).


Notes on Image Copyright Status
===============================

All images used in Mu's developer documentation fall under Mu's copyright
status, except for the following images sourced from third parties:

pygame.png
++++++++++

Permission was sought and obtained from René Dudfield, the current core
maintainer of `Pygame <https://pygame.org>`_::

    public domain

    Go for it! Feel free to do whatever weird(and not weird) things you
    like with it.

    It's a modification (by me) of a logo by Gareth Noyce, who also put
    the logo files in public domain.

    Gareth Noyce said of the logo files:
    They're public domain but I'd like attribution if they're used
    anywhere.  Just a "logo by Gareth Noyce" would do, but I won't be
    complaining if people forget. :)'

lego.png
++++++++

This is a copped version of the
`Creative Commons licensed <https://creativecommons.org/licenses/by-sa/2.0/>`_
photograph taken by
`Jeff Eaton <https://www.flickr.com/people/jeffeaton/>`_
that can be `found here <https://www.flickr.com/photos/jeffeaton/7298224068>`_.

pyboard.png
+++++++++++

The `MicroPython Logo <https://commons.wikimedia.org/wiki/File:MicroPython_new_logo.svg>`_
is covered by the MIT license.

web.png
+++++++

The `Flask Logo <https://commons.wikimedia.org/wiki/File:Flask_logo.svg>`_ has
been released to the public domain.
